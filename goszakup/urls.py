from django.conf.urls import url
from goszakup import views

urlpatterns = [
    url(r'^sistema/$', views.sistema_lots, name='sistema_lots'),
    url(r'^sistema/delete/$', views.sistema_delete, name='sistema_delete'),
    url(r'^bez/$', views.bez_lots, name='bez_lots'),
    url(r'^bez/delete/$', views.bez_delete, name='bez_delete'),
    url(r'^$', views.default_redirect, name='redirect')
]