from django.contrib import admin
from goszakup.models import SistemaLots
from goszakup.models import BezLots


# Register your models here.
admin.site.register(SistemaLots)
admin.site.register(BezLots)