from django.shortcuts import render, redirect
from goszakup.models import SistemaLots
from goszakup.models import BezLots
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import datetime

# Create your views here.
@csrf_exempt
def sistema_lots(request):
    if request.method == 'POST':
        request.session['order_by'] = request.POST.get('order_by')
    sort_by = request.session.get('order_by', default='finish')
    dt = datetime.datetime.today()
    lots = SistemaLots.objects.filter(finish__gt=dt).order_by(sort_by)
    subdiv = 'Система'
    count = lots.count()
    return render(request, 'goszakup/zakupki.html', {'lots': lots, 'subdiv': subdiv, 'total': count, 'order': sort_by})

@csrf_exempt
def bez_lots(request):
    if request.method == 'POST':
        request.session['order_by'] = request.POST.get('order_by')
    sort_by = request.session.get('order_by', default='finish')
    dt = datetime.datetime.today()
    lots = BezLots.objects.filter(finish__gt=dt).order_by(sort_by)
    subdiv = 'Безопасность'
    count = lots.count()
    return render(request, 'goszakup/zakupki.html', {'lots': lots, 'subdiv': subdiv, 'total': count, 'order': sort_by})

def sistema_delete(request):
    lotnum = request.GET.get('lotnum')
    count = int(request.GET.get('count')) - 1
    lot = SistemaLots.objects.get(lotnum=lotnum)
    lot.delete()
    return redirect('../#' + str(count))

def bez_delete(request):
    lotnum = request.GET.get('lotnum')
    count = int(request.GET.get('count')) - 1
    lot = BezLots.objects.get(lotnum=lotnum)
    lot.delete()
    return redirect('../#' + str(count))

def default_redirect(request):
    return redirect('/zakupki/sistema/')
