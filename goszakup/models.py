from django.db import models

# Create your models here.
RATES = (
    (0, 0),
    (1, 1),
    (2, 2),
    (3, 3),
    (4, 4),
    (5, 5)
)

class SistemaLots(models.Model):
    lotnum = models.BigIntegerField(primary_key=True, unique=True, db_column='lotnum', verbose_name='Номер лота', help_text='Номер лота')
    title = models.TextField(verbose_name='Название', help_text='Название', db_column='title')
    price = models.PositiveIntegerField(verbose_name='Цена', help_text='Цена', db_column='price')
    city = models.CharField(max_length=100, verbose_name='Город', help_text='Город', db_column='city')
    start = models.DateTimeField(verbose_name='Начало', help_text='Начало', db_column='start')
    finish = models.DateTimeField(verbose_name='Завершение', help_text='Завершение', db_column='finish')
    link = models.URLField(verbose_name='Ссылка', help_text='Ссылка', db_column='link')
    lot_type = models.CharField(max_length=20, verbose_name='Тип лота', help_text='Тип лота', db_column='lot_type')
    added = models.DateTimeField(verbose_name='Время добавления', help_text='Время добавления', db_column='added')
    notes = models.TextField(blank=True, null=True, verbose_name='Заметки', help_text='Заметки', db_column='notes')
    rate = models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Оценка', help_text='Оценка', db_column='rate', default=0, choices=RATES)

    def __str__(self):
        return str(self.lotnum) + ' - ' + self.title[:100]

    class Meta:
        verbose_name = 'Лоты системы'
        verbose_name_plural = 'Лоты системы'
        ordering = ["finish", "-lotnum", "price"]

class BezLots(models.Model):
    lotnum = models.BigIntegerField(primary_key=True, unique=True, db_column='lotnum', verbose_name='Номер лота', help_text='Номер лота')
    title = models.TextField(verbose_name='Название', help_text='Название', db_column='title')
    price = models.PositiveIntegerField(verbose_name='Цена', help_text='Цена', db_column='price')
    city = models.CharField(max_length=100, verbose_name='Город', help_text='Город', db_column='city')
    start = models.DateTimeField(verbose_name='Начало', help_text='Начало', db_column='start')
    finish = models.DateTimeField(verbose_name='Завершение', help_text='Завершение', db_column='finish')
    link = models.URLField(verbose_name='Ссылка', help_text='Ссылка', db_column='link')
    lot_type = models.CharField(max_length=20, verbose_name='Тип лота', help_text='Тип лота', db_column='lot_type')
    added = models.DateTimeField(verbose_name='Время добавления', help_text='Время добавления', db_column='added')
    notes = models.TextField(blank=True, null=True, verbose_name='Заметки', help_text='Заметки', db_column='notes')
    rate = models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Оценка', help_text='Оценка', db_column='rate', default=0, choices=RATES)

    def __str__(self):
        return str(self.lotnum) + ' - ' + self.title[:100]

    class Meta:
        verbose_name = 'Лоты безопасности'
        verbose_name_plural = 'Лоты безопасности'
        ordering = ["finish", "-lotnum", "price"]