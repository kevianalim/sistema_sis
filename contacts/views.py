from django.shortcuts import render

# Create your views here.
def peoples(request):
    return render(request, "contacts/peoples.html")

def divisions(request):
    return render(request, "contacts/divisions.html")