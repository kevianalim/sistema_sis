from django.conf.urls import url
from contacts import views

urlpatterns = [
    url(r'^peoples$', views.peoples, name='contacts'),
    url(r'^divisions$', views.divisions, name='contacts')
]