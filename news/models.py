from django.db import models

# Create your models here.
class News(models.Model):
    title = models.CharField(max_length=100, db_column='title', verbose_name='Название новости', help_text='Название новости')
    text = models.TextField(db_column='text', verbose_name='Текст новости', help_text='Текст новости')
    added = models.DateTimeField(auto_now_add=True, verbose_name='Дата публикации', help_text='Дата публикации')

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'
