from django.db import models

# Create your models here.
class Help(models.Model):
    title = models.CharField(max_length=100, db_column='title', verbose_name='Название статьи', help_text='Название статьи')
    text = models.TextField(db_column='text', verbose_name='Текст статьи', help_text='Текст статьи')
    added = models.DateTimeField(auto_now_add=True, verbose_name='Дата публикации', help_text='Дата публикации')

    class Meta:
        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи'