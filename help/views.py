from django.shortcuts import render
from django.http import HttpResponse
from help.models import Help

# Create your views here.
def help(request):
    return render(request, "help/help.html")